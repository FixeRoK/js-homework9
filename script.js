function createList(array, parent = document.body) {
    const list = document.createElement('ul');
    parent.append(list);

    array.map((item) => {
        const listItem = document.createElement('li');
        list.append(listItem);

        if (Array.isArray(item)) {
            createList(item, listItem);
        } else {
            listItem.textContent = item;
        }
    });
}


const nestedArray = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
createList(nestedArray);

